$fn = 30;

WIDTH=190;
HEIGHT=120;
THICKNESS=2;
SMOOTH_RAD=0.7;

HOOK_Z_START=60;
HOOK_HEIGHT=30;
HOOK_WIDTH=15;

minkowski() {
    linear_extrude(height=WIDTH - 2*SMOOTH_RAD) {
        union() {
            square([THICKNESS - 2*SMOOTH_RAD, HEIGHT - 2*SMOOTH_RAD]);
            translate([0, HEIGHT - HOOK_Z_START + SMOOTH_RAD, 0])
                rotate(-45)
                square([THICKNESS - 2*SMOOTH_RAD, HOOK_WIDTH * sqrt(2)]);
            translate([HOOK_WIDTH - (THICKNESS - 2*SMOOTH_RAD) * (sqrt(2) - 1) / sqrt(2), HEIGHT - HOOK_Z_START + SMOOTH_RAD + HOOK_WIDTH - (THICKNESS - 2*SMOOTH_RAD) / sqrt(2), 0])
                square([THICKNESS - 2*SMOOTH_RAD, HOOK_HEIGHT - HOOK_WIDTH]);
        }
    };
    sphere(SMOOTH_RAD);
};
