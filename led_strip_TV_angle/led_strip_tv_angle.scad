/*
 * PARAMETERS
 */
base_thickness = 0.5;
angle_radius = 20;
strip_width = 10;
hole_size = 5;
hole_width = 3;

$fn = 50;

/*
 * MODULES
 */
module angle () {
    difference() {
        translate([0, 0, -base_thickness]) cube([angle_radius + strip_width, angle_radius + strip_width, angle_radius + base_thickness]);
        translate([angle_radius + strip_width, -1, angle_radius]) rotate([-90, 0, 0]) cylinder(angle_radius + strip_width + 2, r=angle_radius);
        translate([-1, angle_radius + strip_width, angle_radius]) rotate([0, 90, 0]) cylinder(angle_radius + strip_width + 2, r=angle_radius);
    }
}

module hook () {
    translate([-(hole_size / 2 + hole_width), 0, 0])
        rotate([90, 0, 0])
        rotate_extrude(angle=180, convexity=10)
        translate([hole_size / 2 + hole_width, 0])
        circle(d=hole_width);
    translate([0, 0, -angle_radius])
        cylinder(d=hole_width, angle_radius);
}

/*
 * RENDER
 */
intersection() {
    union() {
        angle();
        translate([angle_radius + strip_width - hole_width / 2, angle_radius + strip_width - hole_width / 2 - sqrt(2) / 2 * (hole_width + hole_size), hole_width])
            rotate([0, 0, -45])
            hook();
        translate([angle_radius / 2 + strip_width - hole_width / 2, angle_radius / 2 + strip_width - hole_width / 2 + sqrt(2) / 2 * (hole_width + hole_size), (angle_radius - angle_radius * sqrt(1 - (angle_radius / 2 / angle_radius)^2)) / 2 + hole_width])
            rotate([0, 0, 135])
            hook();
    }
    translate([0, 0, -base_thickness]) cube([2*(angle_radius + strip_width), 2*(angle_radius + strip_width), angle_radius + base_thickness]);
}
