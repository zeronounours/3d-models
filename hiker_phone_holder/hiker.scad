$fs = 0.5;

/*
 * PARAMETERS
 */
hiker_width = 30;
base_width = 50;
holder_width = 50;
slider_width = 15;

hiker_point_scaling = 3 / 2;
hiker_smooth_radius_arm = 2;
hiker_smooth_radius_body = 3;

pin_depth = 5;
pin_radius = 1;

cable_radius = 2.5;

slider_length = 30;
slider_thickness = 2;
slider_play = 0.1;

holder_length = 80;
holder_smooth_radius = 0.9;
holder_bottom_length = 20;


WITH_HIKER_LEFT = 1;
WITH_HIKER_LEFT_ATTACH = 1;
WITH_HIKER_RIGHT = 1;
WITH_HIKER_RIGHT_ATTACH = 1;
WITH_HIKER_BASE = 1;
WITH_HOLDER = 1;

/*
 * MODELS
 */

// Hiker
module 2d_smooth(smooth_rad) {
    minkowski() {
        children();
        circle(r=smooth_rad);
    }
}
module hiker_extrude(heigth) {
    // do extrusion of hiker's parts
    rotate([90, 0, 90]) linear_extrude(heigth, convexity=4)
        scale([hiker_point_scaling, hiker_point_scaling, hiker_point_scaling])
        children();
}
// left body of the hiker
module hiker_left_body() {
    // body shape
    hiker_extrude(hiker_width / 2) 2d_smooth(hiker_smooth_radius_body)
        /* Original dimension (no rounding)
            [52.5, 5], [59.5, 5], // foot position
            [45, 30], // back of knee
            [51.5, 49], // butt
            [34, 81.5], // back of shoulder
            [22.5, 82.5], // neck
            [19.5, 72], // front of shoulder
            [37.5, 52.5], // lower abdomen
            [38.5, 27], // front of knee
         */
        polygon([
            [40, 29], // front of knee
            [55.5, 5], [56.5, 5], // foot position
            [43, 29], // back of knee
            [50.5, 49], // butt
            [32, 80], // back of shoulder
            [24, 81], // neck
            [21.5, 73.5], // front of shoulder
            [43.9, 46.6], // lower abdomen
        ]);
}
// right body of the hiker
module hiker_right_body() {
    // body shape
    hiker_extrude(hiker_width / 2) 2d_smooth(hiker_smooth_radius_body)
        /* Original dimension (no rounding)
            [25, 5], [32.5, 5], // foot position
            [27, 26.5], // back of knee
            [51.5, 49], // butt
            [34, 81.5], // back of shoulder
            [22.5, 82.5], // neck
            [19.5, 72], // front of shoulder
            [37.5, 52.5], // lower abdomen
            [19.5, 32], // front of knee
        */
        polygon([
            [23.5, 31.5], // front of knee
            [30, 5], [31.5, 5], // foot position
            [26, 30], // back of knee
            [49, 45], // lower part of butt
            [50.5, 49], // butt
            [32, 80], // back of shoulder
            [24, 81], // neck
            [21.5, 73.5], // front of shoulder
            [43.9, 46.6], // lower abdomen
        ]);
}
// left arm of the hiker
module hiker_left_arm() {
    // arm shape
    hiker_extrude(hiker_width / 2) 2d_smooth(hiker_smooth_radius_arm)
        /* Original dimension (no rounding)
            [22.5, 82.5], // neck
            [19.5, 72], // front of shoulder
            [-4, 70], // up of hand
            [-4, 65], // botton of hand
            [22, 65], // elbow
            [29, 73], // middle of shoulder
        */
        polygon([
            [24, 81], // neck
            [21.5, 73.5], // front of shoulder
            [0, 72.5], // up of hand
            [0, 71.5], // botton of hand
            [19, 70.5], // elbow
            [29, 73], // middle of shoulder
        ]);
}
// right arm of the hiker
module hiker_right_arm() {
    // arm shape
    hiker_extrude(hiker_width / 2) 2d_smooth(hiker_smooth_radius_arm)
        /* Original dimension (no rounding)
            [22.5, 82.5], // neck
            [19.5, 72], // front of shoulder
            [25, 66], // inner elbow
            [9, 49], // up of hand
            [11, 45], // botton of hand
            [35, 68.5], // elbow
            [29, 73], // middle of shoulder
        */
        polygon([
            [24, 81], // neck
            [21.5, 73.5], // front of shoulder
            [25.31, 68.93], // inner elbow
            [7, 52], // up of hand
            [7.5, 51.5], // botton of hand
            [31.5, 69], // elbow
        ]);
}
// head of the hiker
module hiker_half_head() {
    hiker_extrude(hiker_width / 4)
        translate([20, 87.4]) circle(r=6);
}
module hiker_left() {
    hiker_left_body();
    hiker_left_arm();
    hiker_half_head();
    // add pins
    translate(hiker_point_scaling * [0, 20, 87.4]) rotate([0, -90, 0]) cylinder(pin_depth, r=pin_radius);
    translate(hiker_point_scaling * [0, 39, 57]) rotate([0, -90, 0]) cylinder(pin_depth, r=pin_radius);
}

module hiker_right() {
    difference() {
        union() {
            translate([-hiker_width / 2, 0, 0]) {
                hiker_right_body();
                hiker_right_arm();
            }
            translate([-hiker_width / 4, 0, 0]) hiker_half_head();
        }
        // add pin holes
        translate(hiker_point_scaling * [0, 20, 87.4]) rotate([0, -90, 0]) cylinder(pin_depth, r=pin_radius);
        translate(hiker_point_scaling * [0, 39, 57]) rotate([0, -90, 0]) cylinder(pin_depth, r=pin_radius);
    }
}

module base() {
    difference() {
        translate([-base_width / 2, 0, 0]) hiker_extrude(base_width)
            polygon([
                [0, 0], [75, 0],
                [75, 5], [0, 15],
            ]);
        hiker_left();
        hiker_right();
    }
}

module rounded_cable_hole(hulled_by=0) {
    translate([0, -50, 0])
        rotate([60, 0, 0])
        rotate([0, 90, 0])
        rotate_extrude(angle=90, convexity=2)
        translate([50, 0])
        hull() {
            circle(r=cable_radius);
            translate([0, hulled_by]) circle(r=cable_radius);
        }
}
module left_arm_cable_translate() {
    translate([hiker_width / 4, 7, 0])
        translate(hiker_point_scaling * [0, 0, 72])
        children();
}
module right_arm_cable_translate() {
    translate([-hiker_width / 4, 23, 0])
        translate(hiker_point_scaling * [0, 0, 38])
        children();
}

/*
 * RENDER
 */
difference() {
    union() {
        if (WITH_HIKER_LEFT) {
            difference() {
                hiker_left();
                // remove part for the attach
                left_arm_cable_translate()
                    rounded_cable_hole(hulled_by=-50);
            }
        }
        if (WITH_HIKER_LEFT_ATTACH) {
            intersection() {
                hiker_left();
                // remove part for the attach
                left_arm_cable_translate()
                    rounded_cable_hole(hulled_by=-50);
            }
        }
        if (WITH_HIKER_RIGHT) {
            difference() {
                hiker_right();
                // remove part for the attach
                right_arm_cable_translate()
                    rounded_cable_hole(hulled_by=50);
            }
        }
        if (WITH_HIKER_RIGHT_ATTACH) {
            intersection() {
                hiker_right();
                // remove part for the attach
                right_arm_cable_translate()
                    rounded_cable_hole(hulled_by=50);
            }
        }
        if (WITH_HIKER_BASE) base();
    }

    // create cable hole in the case
    translate([0, -1, 0]) rotate([-90, 0, 0]) hull() {
        cylinder(150, r=cable_radius);
        translate([0, -cable_radius, 0]) cylinder(150, r=cable_radius);
    }
    // Create hole for the slider of the holder
    translate(hiker_point_scaling * [0, 41.5, 64.5]) rotate([atan2(50.5 - 32, 80 - 49), 0, 0]) {
        translate([-slider_width / 2 - slider_play, 0, 0])
            cube([slider_width + 2 * slider_play, slider_thickness + 2 * slider_play, 200]);
        translate([-3 * slider_thickness / 2 - slider_play, slider_thickness, 0])
            cube([3 * slider_thickness + 2 * slider_play, 2 * slider_thickness, 200]);
    }
    // Create hole for cable in hands
    left_arm_cable_translate() rounded_cable_hole();
    right_arm_cable_translate() rounded_cable_hole();
}
if (WITH_HOLDER) {
    // create the holder
    translate(hiker_point_scaling * [0, 41.5, 64.5]) rotate([atan2(50.5 - 32, 80 - 49), 0, 0]) {
        translate([0, slider_play, 0]) {
            // slider part
            translate([-slider_width / 2, 0, 0])
                cube([slider_width, slider_thickness, slider_length]);
            translate([-3 * slider_thickness / 2, slider_thickness, 0])
                cube([3 * slider_thickness, slider_thickness + slider_play, slider_length]);

            // holder part
            preminkowski_w = holder_width - 2 * holder_smooth_radius;
            preminkowski_d = slider_thickness - 2 * holder_smooth_radius;
            preminkowski_l = holder_length - 2 * holder_smooth_radius;
            preminkowski_bot_l = holder_bottom_length + holder_smooth_radius;
            translate([0, 2 * slider_thickness + slider_play, -preminkowski_l / 2]) minkowski() {
                union() {
                    // circle top
                    translate([0, 0, preminkowski_l - preminkowski_w / 2]) rotate([-90, 0, 0])
                        cylinder(preminkowski_d, r=preminkowski_w / 2);
                    // base part
                    translate([-preminkowski_w / 2, 0, 0])
                        cube([preminkowski_w, preminkowski_d, preminkowski_l - preminkowski_w / 2]);

                    // bottom parts
                    translate([preminkowski_w / 4, preminkowski_d, 0])
                        cube([preminkowski_w / 4, preminkowski_bot_l, preminkowski_d]);
                    translate([-preminkowski_w / 2, preminkowski_d, 0])
                        cube([preminkowski_w / 4, preminkowski_bot_l, preminkowski_d]);
                }
                // make the holder rounder
                translate([0, holder_smooth_radius, 0]) sphere(r=holder_smooth_radius);
            }
        }
    }
}
