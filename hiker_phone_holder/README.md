# Hiker phone holder

A phone holder in the shape of a hiker. Inspired from commercial phone holders.

# Images

## Overview

![overview](images/hiker.png)

## Parts

![hiker base](images/base.png)
![hiker holder](images/holder.png)
![hiker left](images/left.png)
![hiker right](images/right.png)
![hiker left attach](images/left attach.png)
![hiker right attach](images/right attach.png)
