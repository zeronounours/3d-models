# Hand Water Tap

The case and hanger for a hand water tap.

# Requirements

This project relies on https://github.com/adrianschlatter/threadlib which
should be made available in your library.

# Images

![complete example](images/overall.png)

![hand water tap](images/tap.png)

![support](images/support.png)
