include<config.scad>;
include<modules/handle.scad>;
include<modules/tap.scad>;

/*************
 * Parameter *
 *************/
WITH_DECORATION = 1;

/*************
 * 3D Models *
 *************/
handle();

%if (WITH_DECORATION) {
    tap();
}
