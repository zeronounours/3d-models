include<config.scad>;
include<modules/handle.scad>;
include<modules/handle_back.scad>;
include<modules/tap.scad>;

/*************
 * Parameter *
 *************/
WITH_DECORATION = 1;

/*************
 * 3D Models *
 *************/
color("red") handle();
color("yellow") translate([0, 0, -handle_length]) handle_back();

%if (WITH_DECORATION) {
    tap();
}

