use <threadlib/threadlib.scad>

module handle() {
    // base of the handle
    translate([0, 0, -handle_length]) union() {
        difference() {
            cylinder(h=handle_length, r1=handle_bottom_radius, r2=handle_top_radius);

            // empty space for bolt
            translate([0, 0, -1]) cylinder(h=handle_length + 2, r=tap_bolt_radius + slack);

            // empty space for the nut
            translate([0, 0, -1]) cylinder(r=tap_nut_radius + slack, h=tap_nut_height + slack, $fn=6);
        };

        // thread to join with the handle_back
        rotate([180, 0, 0]) nut(handle_back_nut_profile, turns=handle_back_nut_turns, Douter=handle_bottom_radius*2);
    }
}
