use <threadlib/threadlib.scad>

module handle_back() {
    specs = thread_specs(str(handle_back_nut_profile, "-ext"));
    P = specs[0]; Dsupport = specs[2];
    H = (handle_back_nut_turns + 1) * P;

    // strait part
    difference() {
        translate([0, 0, -H]) render(convexity=6) union() {
            bolt(handle_back_nut_profile, turns=handle_back_nut_turns);
            rotate([180, 0, 0]) cylinder(h=handle_back_length - handle_bottom_radius - H, r=handle_bottom_radius);
        }
        translate([0, 0, 1]) rotate([180, 0, 0])
            cylinder(h=handle_back_length + 2, r=Dsupport / 2 - thickness);
    }

    // curved part
    translate([0, 0, -handle_back_length + handle_bottom_radius]) intersection() {
        difference() {
            sphere(r=handle_bottom_radius);
            sphere(r=Dsupport / 2 - thickness);
            cylinder(h=3*handle_bottom_radius, r=handle_back_hole_radius, center=true);
        }
        translate([-handle_bottom_radius, -handle_bottom_radius, -handle_bottom_radius])
            cube([2*handle_bottom_radius, 2*handle_bottom_radius, handle_bottom_radius]);
    }
}

