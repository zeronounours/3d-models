thread_size = 1;

module tap() {
    // base
    cylinder(r=tap_hook_base_radius, h=tap_hook_base_height);
    // hook
    translate([0, 0, tap_hook_base_height]) cylinder(r=tap_hook_radius, h=tap_hook_length);
    translate([tap_hook_overall_radius, 0, tap_hook_base_height + tap_hook_length])
        rotate([-90, 0, 0])
        rotate_extrude(angle=tap_hook_overall_angle, convexity=10)
        translate([-tap_hook_overall_radius, 0]) circle(r=tap_hook_radius);
    // bolt/nut
    translate([0, 0, -tap_bolt_length]) _thread(r=tap_bolt_radius, h=tap_bolt_length);
    translate([0, 0, -handle_length]) _nut(r=tap_nut_radius, h=tap_nut_height, m=tap_bolt_radius);
}

module _nut(r, h, m) {
    difference() {
        cylinder(r=r, h=h, $fn=6);
        translate([0, 0, -1]) _thread(h=h+2, r=m);
    }
}

module _thread(h, r) {
    linear_extrude(height=h, twist=h / thread_size *360)
        translate([thread_size, 0, 0])
        circle(r=r - thread_size / 2);
}
