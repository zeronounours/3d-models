/*****************
 * Configuration *
 *****************/

// Resolution
$fn = 60;

// thickness of borders
thickness = 1.5;
slack = 0.5;

// Tap dimensions
tap_hook_base_radius = 15;
tap_hook_base_height = 20;
tap_hook_radius = 3;
tap_hook_length = 60;
tap_hook_overall_radius = 30;
tap_hook_overall_angle = 160;

tap_bolt_radius = 5;
tap_bolt_length = 56;

tap_nut_radius = 8;
tap_nut_height = 7;

// Handle dimensions
handle_top_radius = tap_hook_base_radius;
handle_bottom_radius = 12;
handle_length = tap_bolt_length - 20;
handle_back_nut_profile = "M22";
handle_back_nut_turns = 5;
handle_back_length = 100 - handle_length;
handle_back_hole_radius = 4;
