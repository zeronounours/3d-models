/*
 * PARAMETERS
 */
// Attach
//attach_base_length = 37;
attach_base_length = 35;
//attach_triangle_heigth = 25;
attach_triangle_heigth = 22;
//attach_rounding_radius = 5;
attach_rounding_radius = 3;
attach_min_height = 5;
attach_max_height = 10;
attach_exit_height = 3;
attach_exit_distance = 12;
attach_exit_thickness = 4;

attach_angle = 20;
border_distance = 80;
border_height = 90;
border_length_1 = 150;
border_length_2 = 70;
border_angle = 45;
border_thickness = 1.5;

$fn = 50;

/*
 * MODULES
 */
// border
module attach() {
    // attach part
    difference() {
        intersection() {
            // triangle part
            translate([attach_rounding_radius, 0, 0])
                linear_extrude(attach_max_height)
                minkowski() {
                    circle(r=attach_rounding_radius);
                    polygon([
                        [0, attach_base_length / 2 - attach_rounding_radius],
                        [0, -attach_base_length / 2 + attach_rounding_radius],
                        [attach_triangle_heigth - 2 * attach_rounding_radius, 0],
                    ]);
                };
            // slope
            rotate([0, -90, 0])
                translate([0, -attach_base_length / 2, -2 * attach_base_length])
                linear_extrude(4 * attach_base_length)
                polygon([
                    [0, 0],
                    [attach_min_height, 0],
                    [attach_max_height, attach_base_length],
                    [0, attach_base_length],
                ]);
        }
        translate([attach_exit_distance, -attach_base_length / 2 - attach_rounding_radius, attach_exit_height])
            cube([attach_exit_thickness, attach_base_length + 2 * attach_rounding_radius, attach_max_height]);
    }
}
module border() {
    // border part
    translate([border_distance, 0, 0])
        rotate([0, 0, attach_angle])
        union() {
            cube([border_thickness, border_length_1, border_height]);
            translate([border_thickness, border_length_1, 0])
                rotate([0, 0, border_angle])
                translate([-border_thickness, 0, 0])
                cube([border_thickness, border_length_2, border_height]);
        };
}
module border_with_attach() {
    translate([attach_base_length, 0, 0]) rotate([0, 0, -attach_angle]) union() {
        attach();
        border();
        // part between the attach & the border
        hull() {
            intersection() {
                attach();
                translate([attach_exit_distance + attach_exit_thickness, -attach_base_length / 2 - attach_rounding_radius, 0])
                    cube([attach_triangle_heigth + 2 * attach_rounding_radius, attach_base_length + 2 * attach_rounding_radius, attach_max_height]);
            }
            translate([border_distance, 0, 0])
                rotate([0, 0, attach_angle])
                cube([border_thickness, border_length_1, 2 * attach_exit_thickness]);
        }
    }
}

/*
 * RENDER
 */
border_with_attach();
mirror([1, 0, 0]) border_with_attach();
