# Deedot empty station borders

Borders to add to the deebot T9 empty station to prevent the robot from
mounting on the station and damaging caoutchouc

# Images

![borders](images/borders.png)
