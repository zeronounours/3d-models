/*
 * PARAMETERS
 */
external_radius = 10;
internal_radius = 3;
axis_length = 10;
button_length = 10;
upper_rounding_radius = 5;
minkowski_radius = 3;
arrow_length = 2;

$fn = 50;

/*
 * MODULES
 */
// button
module button() {
    pre_min_rad = external_radius - minkowski_radius;
    difference() {
        union() {
            minkowski() {
                translate([0, 0, minkowski_radius]) union() {
                    cylinder(button_length - minkowski_radius, r=pre_min_rad);
                    translate([0, 0, button_length - minkowski_radius]) scale([1, 1, upper_rounding_radius / pre_min_rad]) sphere(pre_min_rad);
                }
                sphere(minkowski_radius);
            }
            linear_extrude(button_length)
                polygon([[external_radius + arrow_length, 0], [0, external_radius / 2], [0, -external_radius / 2]]);
        }
        translate([0, 0, -1]) cylinder(axis_length + 1, r=internal_radius);
    }
}

/*
 * RENDER
 */
button();
