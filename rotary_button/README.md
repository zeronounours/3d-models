# Rotary button

A rotary button to place on top of a potentiometer.

# Images

![button](images/button.png)
