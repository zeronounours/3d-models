# Grommet

A grommet to make holes more beautiful

# Images

## Full grommet

![full grommet](images/full.png)

## Half grommet

![half grommet](images/half.png)
