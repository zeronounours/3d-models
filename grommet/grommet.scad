// Parameters
width = 58;
depth = 27;
heigth = 30;
thickness = 2;
support_width = 10;
extrude_angle = 180;

$fs = 0.5;

WITH_DECORATION = 1;

// Models

wradius = width / 2;
// table with hole
if (WITH_DECORATION) {
    % difference() {
        translate([-width, 0, -heigth]) cube([2 * width, 2 * depth, heigth]);
        translate([0, 0, -heigth - 1]) scale([1, depth / wradius, 1]) cylinder(heigth + 2, r=wradius);
    }
}

// Grommet
union() {
    scale([1, depth / wradius, 1])
        rotate_extrude(angle=extrude_angle)
        union() {
            // base 2D shape
            polygon([
                [wradius - thickness, -heigth + thickness],
                [wradius, -heigth + thickness],
                [wradius, 0],
                [wradius + support_width - thickness, 0],
                [wradius + support_width - thickness, thickness],
                [wradius - thickness, thickness],
            ]);
            // add rounder corners
            translate([wradius, -heigth + thickness]) intersection() {
                circle(r=thickness);
                translate([-thickness, -thickness]) square(thickness);
            }
            translate([wradius + support_width - thickness, 0]) intersection() {
                circle(r=thickness);
                square(thickness);
            }
        }
}
