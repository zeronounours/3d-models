/*
 * PARAMETERS
 */
spice_recipient_radius = 21;
holder_base_height = 5;
holder_height_percent = 0.5;
holder_depth = 40;
holder_hole_count = 6;

$fn = 50;

/*
 * MODULES
 */
// One spice recipient holder
module holder() {
    difference() {
        cube([holder_height_percent * spice_recipient_radius + holder_base_height, 2 * spice_recipient_radius, holder_depth]);
        translate([spice_recipient_radius + holder_base_height, spice_recipient_radius, -1]) cylinder(holder_depth + 2, r=spice_recipient_radius);
    }
}

/*
 * RENDER
 */
for (i = [1:holder_hole_count] ) translate([0, (i - 1) * 2 * spice_recipient_radius, 0]) holder();
