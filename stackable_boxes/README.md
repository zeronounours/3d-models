# Stackable boxes

Boxes which can be stacked to place items.

# Images

## Overall

![overall view](images/overall.png)

## Lower boxes

![lower box](images/lower.png)

## Middle boxes

![lower box](images/middle.png)

## Upper boxes

![upper box](images/upper.png)
