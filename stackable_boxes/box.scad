/*
 * PARAMETERS
 */
// Length parameters
width = 190;
depth = 220;
thickness = 2;
upper_thickness = 2 * thickness;
cable_hole_radius = 5;

// List of boxes height (from lower to upper)
boxes = [30, 80, 80];

// weither to add cable hole on lower case
lower_with_cable_hole = 1;
heigth = 120;

$fs = 0.5;

// Display options
// Additional distance to add between boxes
SPLIT = 0;

// if set to a valid index, only shows this box
SHOW_ONLY = -1;

/*
 * MODULES
 */
// Box model
module box(heigth, is_top=0, with_cable_hole=0) {
    difference() {
        // base shape
        cube([depth, width, heigth]);

        // remove inner: keep front opened
        translate([thickness, thickness, -1])
            cube([depth, width - 2 * thickness, heigth - upper_thickness + 1]);

        // make a upper border to clip
        if (!is_top) {
            translate([-1, -1, heigth - thickness]) difference() {
                cube([depth + 2, width + 2, 2 * thickness]);
                translate([thickness + 1, thickness + 1])
                    cube([depth, width - 2 * thickness, 2 * thickness]);
            }
        }

        // make cable hole
        if (with_cable_hole) {
            translate([-thickness, width / 2, 0]) rotate([0, 90, 0]) hull() {
                cylinder(3 * thickness, r=cable_hole_radius);
                translate([-cable_hole_radius, 0, 0])
                    cylinder(3 * thickness, r=cable_hole_radius);
            }
        }
    }
}

/*
 * RENDER
 */
// render the box of given number
module box_by_num(idx) {
    box(boxes[idx], idx == len(boxes) - 1, lower_with_cable_hole && idx == 0);
}
function add_slice(v, end, i = 0, r = 0) = i < min(len(v), end) ? add_slice(v, end, i + 1, r + v[i]) : r;

if (SHOW_ONLY >= 0 && SHOW_ONLY < len(boxes)) {
    box_by_num(SHOW_ONLY);
} else {
    for (i = [0 : len(boxes) - 1]) {
        translate([0, 0, add_slice(boxes, i) + i * (SPLIT - thickness)])
            box_by_num(i);
    }
}
