// Parameters
sonoff_width = 39.1;
sonoff_height = 31.9;
sonoff_depth = 18.4;
sonoff_border_radius = 3;

case_blocker_return = 1;

wire_protection_height = 10;

thickness = 2;
gap = 0.25;

$fs = 0.5;
$fa = 5;

WITH_DECORATION = 1;

// Models
module rounded_cube(width, height, depth, r) {
    minkowski() {
        translate([-width/2+r, -height/2+r, 0])
            cube([width - 2 * r, height - 2 * r, depth - 2]);
        cylinder(r=r, 2);
    }
}

module case() {
    width = sonoff_width + 2*gap + 2*thickness;
    height = sonoff_height + 2*gap + 2*thickness;
    depth = sonoff_depth + gap + thickness;

    protection_width = width - 2*sonoff_border_radius;

    difference() {
        // base cube for sonoff + wire protection part
        translate([0, wire_protection_height/2, 0])
            rounded_cube(width, height + wire_protection_height, depth, sonoff_border_radius);

        // hole for the sonoff
        translate([0, 0, -gap]) rounded_cube(sonoff_width + 2*gap, sonoff_height + 2*gap, sonoff_depth + 2*gap, sonoff_border_radius);
        // hole above the sonoff
        rounded_cube(sonoff_width - 2*case_blocker_return, sonoff_height - 2*case_blocker_return, 2*(sonoff_depth + gap), sonoff_border_radius);
        // hole for wires
        translate([-protection_width/2+thickness, 0, thickness])
            cube([protection_width - 2*thickness, height + wire_protection_height + 1, depth - 2*thickness]);
    }
}

module sonoff() {
    rounded_cube(sonoff_width, sonoff_height, sonoff_depth, sonoff_border_radius);
}


case();

if (WITH_DECORATION) {
    %sonoff();
}
