// Parameters
// sis
button_width = 80;
button_height = 80;
button_depth = 25;

hole_width = 50;
hole_height = 60;
hole_depth = 21;

thickness = 3;

hole_screw_size = 3;
support_screw_size = 3;
support_screw_radius = 30;

$fs = 0.5;
$fa = 5;

WITH_DECORATION = 1;
WITH_BUTTON = 1;
WITH_WALL = 1;

// Models
module foot() {
    width = hole_width - 1;
    difference() {
        translate([-width / 2, 0, 0]) cube([width, hole_screw_size * 2 + thickness, thickness]);
        translate([- width / 4, thickness + hole_screw_size, -thickness]) cylinder(3*thickness, d=hole_screw_size);
        translate([width / 4, thickness + hole_screw_size, -thickness]) cylinder(3*thickness, d=hole_screw_size);
    };
    translate([-width / 2, 0, 0]) cube([width, thickness, hole_depth]);
}

module switch_support() {
    support_height = 1 + thickness + button_depth - hole_depth;

    translate([0, 0, hole_depth + support_height / 2])
        difference() {
            cube([button_width, button_height, support_height], center=true);
            // center hole
            cube([hole_width, hole_height - 1 - 2 * thickness, support_height + 1], center=true);
            // screw holes
            for (i = [0:180:360]) {
                rotate([0, 0, i])
                    translate([support_screw_radius, 0, 0]) {
                      cylinder(support_height + 1, d=support_screw_size, center=true);
                    }
            }
        }
}

module switch() {
    difference() {
        union() {
            intersection() {
              cylinder(25, d=52);
              translate([-25, -23, 0]) cube([50, 50, 25]);
            }
            translate([0, 0, 29]) cube([81, 81, 8], center=true);
        }
        for (i = [0:90:360]) {
            rotate([0, 0, i])
                translate([support_screw_radius, 0, 0]) {
                  cylinder(50, d=4);
                }
        }
    }
}


// Grommet
union() {
    translate([0, -(hole_height-1) / 2, 0]) foot();
    translate([0, (hole_height-1) / 2, 0]) rotate([0, 0, 180]) foot();
    switch_support();
}

if (WITH_DECORATION) {
    if (WITH_BUTTON)
        %translate([0, 0, thickness + 1]) switch();
    if (WITH_WALL)
        %difference() {
            cube([100, 100, 42], center=true);
            translate([-25, -55, 20 - 8])
                cube([50, 110, 10]);
            translate([-25, -30, 0])
                cube([50, 60, 40]);
        }
}
