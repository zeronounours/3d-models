include<edges.scad>;

/*************
 * Dispenser *
 *************/
module _slope_shaper_side_view(r1, r2, length, extra_width) {
    width = 2 * (max(r1, + r2) + extra_width);
    translate([-width / 2, -r1, 0])
    rotate([90, 0, 90])
    linear_extrude(height=width)
        polygon([
                [-extra_width, thickness],
                [-extra_width, -dispenser_height],
                [r1 + length, -dispenser_height - tan(dispenser_slopes_angle) * (length + r1)],
                [r1 + length + r2 + extra_width, -dispenser_height - tan(dispenser_slopes_angle) * (length + r1)],
                [r1 + length + r2 + extra_width, thickness],
        ]);
}
module _slope_shaper_above_view(r1, r2, length, extra_width) {
    height = thickness + dispenser_height + tan(dispenser_slopes_angle) * (dispenser_length + r1);
    translate([0, 0, thickness])
    rotate([0, 180, 0])
    linear_extrude(height=height)
        hull() {
            circle(r=r1 + extra_width);
            translate([0, r1]) circle(r=r1 + extra_width);
            translate([0, length]) circle(r=r2 + extra_width);
        }
}
module _slope_shaper(r1, r2, length, extra_width) {
    intersection() {
        _slope_shaper_side_view(r1, r2, length, extra_width);
        _slope_shaper_above_view(r1, r2, length, extra_width);
    }
}

module dispenser() {
    // grommet use rotate extrude to keep the tore opened to help adding the
    // attach
    rotate([0, 0, 90 + (360 - grommet_angle_extrude) / 2])
        rotate_extrude(angle=grommet_angle_extrude)
        translate([outer_connection_radius + gap, 0, 0])
        square([grommet_radius - outer_connection_radius - gap, thickness]);

    attach_rad = outer_connection_radius + edge_size + gap;
    slope_height_max = dispenser_height + tan(dispenser_slopes_angle) * (attach_rad + dispenser_length);

    difference() {
        union() {
            _slope_shaper(attach_rad, dispenser_out_rad, dispenser_length, thickness);
            // output cylinder
            rotate([0, 180, 0]) translate([0, dispenser_length, 0]) cylinder(dispenser_out_length + slope_height_max, r=dispenser_out_rad + thickness);
        }
        translate([0, 0, thickness]) _slope_shaper(attach_rad, dispenser_out_rad, dispenser_length, 0);
        // output hole
        rotate([0, 180, 0]) translate([0, dispenser_length, -1]) cylinder(dispenser_out_length + slope_height_max + 2, r=dispenser_out_rad);
    }
}

