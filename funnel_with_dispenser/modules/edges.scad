/*********
 * Edges *
 *********/
module _edge(with_gaps) {
    angle_gap = asin(gap / outer_connection_radius);
    linear_extrude(height=edge_thickness + with_gaps * gap)
        polygon((outer_connection_radius / cos(edge_length_angle / 2)) * [
            [0, 0],
            [sin(edge_length_angle / 2 + with_gaps * gap), cos(edge_length_angle / 2)],
            [-sin(edge_length_angle / 2 + with_gaps * gap), cos(edge_length_angle / 2)],
        ]);
}
module edge_shaper(with_gaps=0) {
    for (i = [0:4]) {
        rotate([0, 0, i * 90 - 90]) _edge(with_gaps);
    }
}
