include<edges.scad>;

/********************
 * Dispenser Attach *
 ********************/
module dispenser_attach() {
    full_height = dispenser_connection_height + edge_thickness + thickness;
    // circular part for the edges
    difference() {
        cylinder(full_height, r=outer_connection_radius);
        translate([0, 0, -1])
            cylinder(full_height + 2, r=outer_connection_radius - thickness);
    }

    // edge to bind w/ funnel
    translate([0, 0, full_height - edge_thickness]) intersection() {
        difference() {
            cylinder(edge_thickness, r=outer_connection_radius);
            translate([0, 0, -1])
                cylinder(edge_thickness + 2, r=outer_connection_radius - thickness - edge_size);
        }
        edge_shaper();
    }

    // edge to bind w/ dispenser
    difference() {
        cylinder(edge_thickness, r=outer_connection_radius + edge_size);
        translate([0, 0, -1])
            cylinder(edge_thickness + 2, r=outer_connection_radius );
    }
}

