include<edges.scad>;

/**********
 * Funnel *
 **********/
module funnel() {
    // circular part for the edges
    translate([0, 0, -funnel_connection_height]) difference() {
        cylinder(funnel_connection_height, r=inner_connection_radius + thickness);
        translate([0, 0, -1])
            cylinder(funnel_connection_height + 2, r=inner_connection_radius);
    }
    // grommet
    difference() {
        cylinder(thickness, r=grommet_radius);
        translate([0, 0, -1]) cylinder(thickness + 2, r=inner_connection_radius);
    }
    // funnel part
    translate([0, 0, thickness]) rotate_extrude(angle=360)
        polygon([
            [inner_connection_radius, 0],
            [inner_connection_radius + thickness, 0],
            [inner_connection_radius + thickness + funnel_height / tan(funnel_slopes_angle), funnel_height],
            [inner_connection_radius + funnel_height / tan(funnel_slopes_angle), funnel_height],
        ]);
    // edge to bind parts together
    translate([0, 0, -funnel_connection_height]) difference() {
        difference() {
            cylinder(funnel_connection_height, r=inner_connection_radius + thickness + edge_size);
            translate([0, 0, -1])
                cylinder(funnel_connection_height + 2, r=inner_connection_radius);
        }
        for (i = [-1:1])
            translate([0, 0, i * edge_thickness]) edge_shaper(with_gaps=1);
        rotate([0, 0, edge_length_angle * 0.90]) translate([0, 0, edge_thickness]) edge_shaper(with_gaps=1);
    }
}
