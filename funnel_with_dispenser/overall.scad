include<config.scad>;
include<modules/dispenser.scad>;
include<modules/dispenser_attach.scad>;
include<modules/funnel.scad>;

/*************
 * 3D Models *
 *************/
translate([0, 0, -edge_thickness - thickness]) dispenser_attach();
translate([0, 0, -thickness]) dispenser();
rotate([0, 0, -0.9 * edge_length_angle]) translate([0, 0, connection_height]) funnel();

// wood plank with the hole
size = 2 * (dispenser_length + outer_connection_radius);
%difference() {
    translate([-size / 2, -size / 2, 0]) cube([size, size, connection_height]);
    translate([0, 0, -1]) cylinder(connection_height + 2, r=outer_connection_radius);
}
