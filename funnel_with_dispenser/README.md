# Funnel with dispenser

A funnel with a hole to a dispenser to go through layers of wood.

# Images

## Overall

![overall view](images/overall.png)

## Funnel

![funnel view](images/funnel.png)

## Dispenser

![dispenser view](images/dispenser.png)

## Dispenser attach

![dispenser attach view](images/dispenser_attach.png)
