/*****************
 * Configuration *
 *****************/
/*
 * Global
 */

// Resolution
$fn = 60;

// Gaps to bound parts together
gap = 0.5;

// thickness of sides
thickness = 1.5;

// Size of the edge to bind parts together
edge_size = 3;
edge_thickness = 2;
edge_length_angle = 30;

// angle for slopes for food (angle with horizontal plane)
funnel_slopes_angle = 45;
dispenser_slopes_angle = 30;

// External radius at the circular section where parts bound together
outer_connection_radius = 44;

// radius & length of the dispenser output
dispenser_out_rad = 25;
dispenser_out_length = 20;

// heights of the different parts
funnel_height = 30;
connection_height = 19;
dispenser_height = 20;

// length of the dispenser slope
dispenser_length = 100;

// Radius for the grommet attached to the funnel
grommet_radius = 55;
grommet_angle_extrude = 360 * 0.75;


/***************
 * Computation *
 ***************/

// Inner radius at the circular section where parts are bound together
inner_connection_radius = outer_connection_radius - 2 * thickness - edge_size - gap;

// height of the connection for all parts
funnel_connection_height = connection_height / 2 + edge_thickness + gap;
dispenser_connection_height = connection_height / 2 + edge_thickness + gap;

// echo computed dimensions
echo(inner_connection_radius=inner_connection_radius);
echo(funnel_connection_height=funnel_connection_height);
echo(dispenser_connection_height=dispenser_connection_height);
