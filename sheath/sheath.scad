LENGTH=200;
WIDTH=115;
HEIGHT=40;
THICKNESS=1;

HOLES_WIDTH=15;
HOLES_SIZE=2;

HOLES_DISTANCE=2 * HOLES_WIDTH;

holes_count=ceil(LENGTH / HOLES_DISTANCE);

difference() {
    // steath
    cube([WIDTH, LENGTH, HEIGHT]);
    translate([THICKNESS, -1, THICKNESS]) cube([WIDTH - 2 * THICKNESS, LENGTH + 2, HEIGHT]);

    // holes
    translate([0, LENGTH / 2 - ((holes_count - 1) * HOLES_DISTANCE + HOLES_WIDTH) / 2, 0])
    for (i = [0:holes_count - 1]) {
        // holes on edges
        for (j = [1:2])
            translate([-1, i * HOLES_DISTANCE, j * HEIGHT / 3]) cube([WIDTH + 2, HOLES_WIDTH, HOLES_SIZE]);
        // holes on bottom
        for (j = [1:4])
            translate([j * WIDTH / 5, i * HOLES_DISTANCE, -1]) cube([HOLES_SIZE, HOLES_WIDTH, THICKNESS + 2]);
    }
}
